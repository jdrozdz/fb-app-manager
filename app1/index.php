<?php 
require_once dirname(__FILE__).'/config/application.php';
require_once dirname(__FILE__).'/config/fb.php';
require_once dirname(__FILE__).'/../lib/Autoload.php';
require_once dirname(__FILE__).'/../lib/ViewLoader.php';

$quiz = new QuizController();
$FB->setAppKeys('app1');
$fb = new Facebook(array("appId" => $FB->appId, "secret" => $FB->appKey));
$user = $fb->getUser();
if($user){
    $head = "<a class=\"pure-button\" href=\"{$fb->getLogoutUrl()}\">Wyloguj</a><br />";
    $me = $fb->api("/me?fields=email,name");
    $appInfo = $fb->api($FB->appId);
    $likes = $fb->api("/$user/likes/141710582694023");
}else{
    $head = '<a class="pure-button" href="'.$fb->getLoginUrl().'">Zaloguj si&#261; na FB</a>';
}

if(!empty($_GET['space'])){
    $tpl = $view->loadTemplate('index/index.twig');
    $questions = $quiz->index($FB->appId);
    echo $tpl->render(array(
            'quest' => $questions, 
            "questModel" => new Model_QuestionsModel(), 
            "user" => $user ,
            "fbfun" => count($likes),
            "head" => $head,
            "me" => $me
        ));
}else{
    $tpl = $view->loadTemplate('index/quiz.twig');
    echo $tpl->render(array("quizes" => new Model_QuizModel(), "code" => ( (!empty($_GET['code'])) ? $_GET['code'] : "" )));
}
?>