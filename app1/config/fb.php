<?php
class FacebookApps {
    
    private $apps = array("app1" => array( 
            "appId" => "175031306010164"
            , "appKey" => "7581a475d8cbd1d9bf03b79fd016c9fe"
            , "clientKey" => "726444710afea770949a02ffd40f3a8f"
        ));
    
    public $appId = null;
    public $appKey = null;
    public $clientKey = null;
    
    public function setAppKeys($applicationName){
        if(count($this->apps[$applicationName]) > 0){
            $this->appId = $this->apps[$applicationName]["appId"];
            $this->appKey = $this->apps[$applicationName]["appKey"];
            $this->clientKey = $this->apps[$applicationName]["clientKey"];
        }else{
            echo ("<b>[Warning][FB]</b> Application does not exists!<br />");
        }
    }
}

$FB = new FacebookApps();

?>
