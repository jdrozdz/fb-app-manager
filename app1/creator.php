<?php
error_reporting(E_ALL);
header("Content-Type: text/plain; charset=utf-8");
require_once dirname(__FILE__).'/config/application.php';
require_once dirname(__FILE__).'/config/fb.php';
require_once dirname(__FILE__).'/../lib/Autoload.php';

$controller = new QuizController();
$userController = new UserController();

switch($_POST["action"]){
    case "addQuiz":
        $id = $controller->addQuiz();
        echo "{ id : {$id}, result : \"ok\" }";
    break;
    case "addQuestion":
        $id = $controller->addQuestion();
        $result = (!empty($_POST["parent_id"])) ? "Odpowiedz zostala dodana" : "Pytanie zostalo dodane";
        echo "{ id : {$id}, result : \"{$result}\", status : \"ok\" }";
        break;
    case 'addUser':
        $id = $userController->addUser();
        echo "{ id : \"{$id}\" }";
        break;
    case 'getAnswer':
        $a = $controller->getAnswer();
        echo " { answer : \"{$a}\" } ";
        break;
    default:
        echo "{ id : 0,result : \"Blad dodania pozycji\", status : \"faild\" }";
}