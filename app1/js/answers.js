var result = 0;
function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
function setUserId(_id){
    $.post("creator.php", { action: "addUser", email : $("#emailaddress").val(), username : $("#username").val(), fb_account : $("#account").val() }, function(data){
        var obj = eval('('+data+')');
        if(parseInt(obj.id) > 0){
            $("#user_id").val(obj.id);
            $("#emailaddress").attr("type", "hidden");
            $("#"+_id).toggle();
            $("#quiz").toggle(1500);
        }else{
            $("<div title=\"Błąd\"></div>").html("Twój adres e-mail lub konto FB już istnieje w bazie<br />Udział w konkursie możesz wziąść tylko <b><i>1 raz</i></b>").dialog({
            model : true
            ,buttons : [ 
                { 
                    text: "OK",
                    click : function(){
                        $(this).dialog("destroy");
                    }
                } 
            ] 
        });
        }
    });
}
$(document).ready(function(){
    $(".answers").click(function(){
        $.post("creator.php", { question_id : parseInt($(this).val()), action : "getAnswer", user_id : $("#user_id").val() }, function(data){
            var obj = eval('('+data+')');
            result += parseInt(obj.answer);
        });
        $("input[name="+$(this).attr("name")+"]").attr("disabled","disabled");
    });

    $("#answer_btm").click(function(){
        var msg;
        var ql = parseInt($(".question").length);
        if(result < ql){
            msg = "Przykro nam, nie usdzielno prawidłowidłowych odpowiedzi na wszystkie pytania";
        }else if(ql == result && result > 0){
            msg = "Gratulacje! Wszytkie odpowidzi są poprawne!";
        }
        $("<div title=\"Twój wynik\"></div>").html(msg).dialog({
            model : true
            ,buttons : [ 
                { 
                    text: "OK",
                    click : function(){
                        $(this).dialog("destroy");
                    }
                } 
            ] 
        });
    });
    
    var mail = $("#emailaddress");
    if(mail.attr("type") == "text"){
        mail.keyup(function(){
            if(IsEmail($(this).val())){
                $("#email-send").css("display","block");
            }
        });
    }
});