$("#next").click(function(){
        var q_name = $("#quiz_name").val();
        if(q_name.length > 0){
            $.ajax({
                url : "creator.php",
                data : { action : "addQuiz", name : q_name },
                type : "POST",
                dataType : "text",
                success : function(obj){
                    var obj = eval('('+obj+')');
                    if(obj.result == "ok"){
                        $().val("");
                        $.get("views/admin/js/questions.html", function(data){
                            var n = data.replace(/\#no\#/g, obj.id);
                            $("<div title='Kreator pytan'></div>").html(n).dialog({
                                modal: true, 
                                width :850, 
                                height: 550, 
                                buttons: [
                                    {
                                        text: "Zapisz",
                                        click: function(){
                                            var quiz_id = $("#quiz_id").val();
                                            var parent_id = $("#parent_id").val();
                                            if(parseInt(parent_id) > 0){
                                                // dodaj odpowiedz
                                                $.post("creator.php", { action : 'addQuestion', question: $("#question").val(), quiz_id : quiz_id, parent_id: parent_id, is_correct : $("#is_correct").val() },function(data){
                                                    var obj = eval('('+data+')');
                                                    alert(obj.result);
                                                    $("#question").val("");
                                                });
                                            }else{
                                                // dodaj pytanie
                                                $.post("creator.php", { action : 'addQuestion', question : $("#question").val(), quiz_id : quiz_id, question_type : $("#type_question").val()},
                                                    function(data){
                                                        var obj = eval('('+data+')');
                                                        if(obj.status == "ok"){
                                                            alert(obj.result);
                                                            $("#question_label").val($("#question").val());
                                                            $("#parent_id").val(obj.id);
                                                            $("#question").val("").attr("placeholder", "Wpisz odpowiedz....");
                                                            $("#change_type_of_form").attr("disable");
                                                            $(".question_type").toggle(1500);
                                                            $(".answer").toggle(1500);
                                                        }
                                                    });
                                            }
                                        }
                                    },
                                    {
                                        text : "Dodaj pytanie",
                                        id : "change_type_of_form",
                                        click : function(){
                                            var question_type = $(".question_type").css("display");
                                            var ch_btm = $("#change_type_of_form");
                                            if(question_type == "none"){
                                                ch_btm.attr("disable");
                                                $("#parent_id").val("");
                                                $(".question_type").toggle(1500);
                                                $(".answer").toggle(1500);
                                            }
                                        }
                                    },
                                    { 
                                        text: 'Zakoncz', 
                                        click: function(){ 
                                            $(this).dialog('destroy'); 
                                        } 
                                    } 
                                ] 
                            });
                        });
                    }
                },
                error: function(data){
                    console.info("ERROR");
                    console.info(data);
                }
            });
        }else{
            $("<div></div>").html("Pole nazwy nie może być puste!").dialog({ 
                    modal: true, 
                    buttons: [ { 
                            text: "OK", click: function () { $(this).dialog("destroy"); }
                        }
                    ]
                });
        }
    });