<?php
class UserController extends Controller{
    private $model;
    public function init(){
        $this->model = new Model_UserModel();
    }
    
    public function __construct() {
        parent::__construct();
        $this->init();
    }
    
    public function userList(){
        
    }
    
    public function addUser(){
        $username = $this->_request->username;
        $fb_account = $this->_request->fb_account;
        $email = $this->_request->email;
        
        return $this->model->addUser($username, $fb_account, $email);
    }
}
