<?php
class QuizController extends Controller{
    
    protected $_models = array();
    protected $employeModel;
    
    public function init(){
        $this->_models["quiz"] = new Model_QuizModel();
        $this->_models["question"] = new Model_QuestionsModel();
        $this->_models["answer"] = new Model_AnswersModel();
        $this->employeModel = new Model_EmployeModel();
    }
    
    public function __construct() {
        parent::__construct();
        $this->init();
    }
    
    public function index($appId){
        $quiz = array();
        $q = $this->_models["quiz"]->getQuizByFBId($appId);
        return $this->_models["question"]->getQuestions($q->quiz_id);
    }

    public function addQuiz(){
        $id = $this->_models["quiz"]->addQuiz($this->_request->name, ( (empty($this->_request->enabled)) ? 1 : 0 ));
        return $id;
    }
    
    public function addQuestion(){
        $quiz_id = $this->_request->quiz_id;
        $question = $this->_request->question;
        @$question_type = $this->_request->question_type;
        
        // odp
        @$is_correct = (empty($this->_request->is_correct)) ? 0 : 1;
        @$parent_id = $this->_request->parent_id;
        
        if($parent_id > 0){
            $id = $this->_models["question"]->addQuestion($quiz_id, $question, 
                    $is_correct, $parent_id, 0);
        }else{
            $id = $this->_models["question"]->addQuestion($quiz_id, $question, 
                    0, 0, $question_type);
        }
        return $id;
    }
    
    public function getAnswer(){
        $question_id = $this->_request->question_id;
        $user_id = $this->_request->user_id;
        $id = $this->_models['question']->getAnswer($question_id);
        $this->_models['answer']->setAnswer($id->quiz_id, $id->question_id, $id->is_correct, $user_id);
        return $id->is_correct;
    }
}
?>
