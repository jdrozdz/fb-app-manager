<?php
class Model_UserModel extends Db_Database{
    protected $_name = "fb_users";
    protected $_id = "user_id";
    protected $_primary = "fb_users_user_id_seq";
    
    public function getAllUsers(){
        $sql = "SELECT * FROM {$this->_name} ORDER BY {$this->_id}";
        $query = $this->prepare($sql);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_CLASS, "Model_Struct_UserManager");
    }
    
    /**
     * Dodaje nowego uzytkonika
     * 
     * @param string $username
     * @param string $fb_account
     * @param string $email
     * @return int
     */
    public function addUser($username,$fb_account, $email){
        $sql = "INSERT INTO fb_users(fb_name, fb_account, email) VALUES(:username, :fb_account, :email)";
        if(count($this->getUserByEmail($email)) == 0){
            $query = $this->prepare($sql);
            $query->bindValue(":username", $username,PDO::PARAM_STR);
            $query->bindValue(":fb_account", $fb_account,PDO::PARAM_INT);
            $query->bindValue(":email", $email,PDO::PARAM_STR);
            $query->execute();
            return $this->lastInsertId($this->_primary);
        }else{
            return 0;
        }
    }
    
    public function getUserByEmail($email){
        $sql = "SELECT * FROM {$this->_name} WHERE email=:email";
        $query = $this->prepare($sql);
        $query->bindValue(":email", $email, PDO::PARAM_STR);
        $query->execute();
        return $query->fetchAll();
    }
}