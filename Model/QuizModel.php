<?php
class Model_QuizModel extends Db_Database {
    protected $_name = "fb_quiz";
    protected $_primary = "fb_quiz_quiz_id_seq";
    
    public function init(){
        if(!$this->_connected){
            parent::__construct();
        }
    }
    
    public function getQuizByFBId($id){
        $sql = "SELECT * FROM {$this->_name} WHERE fb_id='".htmlspecialchars($id)."'";
        $query = $this->prepare($sql);
        $query->execute();
        $r = $query->fetchAll(PDO::FETCH_CLASS, "Model_Struct_QuizModel");
        return $r[0];
    }
    /**
     * Aktualnie aktywne quizy
     * 
     * @param int $enabled Prarmetr przyjmuje wartosci 1 lub 0 ewentualnie null
     * @return object Model_Struct_QuizModel
     */
    public function getQuizes($enabled = 1){
        $query = "SELECT * FROM ".$this->_name." WHERE ".(($enabled == null) ? "" : "is_enable={$enabled}")." ORDER BY quiz_id ASC";
        
        $sql = $this->prepare($query);
        $sql->execute();
        return $sql->fetchAll(PDO::FETCH_CLASS, 'Model_Struct_QuizModel');
    }
    
    public function addQuiz($quiz_name, $enabled = 1){
        $sql = "INSERT INTO {$this->_name}(quiz_id, quiz_name, is_enable) VALUES(DEFAULT, '".  htmlspecialchars($quiz_name)."', '{$enabled}')";
        $query = $this->prepare($sql);
        try{
            $query->execute();
        }  catch (PDOException $e){
            echo $e->getMessage();
        }
        return $this->lastInsertId("fb_quiz_quiz_id_seq");
    }
}
?>
