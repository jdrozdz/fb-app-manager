<?php
class Model_AnswersModel extends Db_Database {
    protected $_name = "fb_answers";
    protected $_id = "answer_id";
    protected $_primary = "fb_answers_answer_id_seq";
    
    public function setAnswer($quiz_id, $question_id, $pass, $user_id){
        $sql = "INSERT INTO {$this->_name}(quiz_id, question_id, pass, user_id) 
                VALUES(:quiz_id, :question_id, :pass, :user_id)";
        $query = $this->prepare($sql);
        $query->bindParam(":quiz_id", $quiz_id, PDO::PARAM_INT);
        $query->bindParam(":question_id", $question_id, PDO::PARAM_INT);
        $query->bindParam(":pass", $pass, PDO::PARAM_INT);
        $query->bindParam(":user_id", $user_id, PDO::PARAM_INT);
        $query->execute();
        return $this->lastInsertId($this->_primary);
    }
}