<?php
class Model_EmployeModel extends Db_Database {
    protected $_name = "fb_employe";
    protected $_id = "employe_id";
    protected $_primary = "fb_employe_employe_id_seq";
    
    public function addEmploye($username, $email, $password, $firstname, $lastname){
        $sql = "INSERT INTO {$this->_name}(username, email, password, firstname, lastname) VALUES ";
        $sql .= "(:username, :email, md5(:password), :firstname, :lastname)";
        $query = $this->prepare($sql);
        $query->bindValue(":username", $username)
              ->bindValue(":email", $email)
              ->bindValue(":password", $password)
              ->bindValue(":firstname", $firstname)
              ->bindValue(":lastname", $lastname)
              ->execute();
        return $this->lastInsertId($this->_primary);
    }
    
    public function removeEmploye($id){
        $sql = "DELETE FROM {$this->_name} WHERE id=:id";
        $query = $this->prepare($sql);
        $query->bindValue(":id", $id);
        $query->execute();
        return ;
    }
    
    public function checkUser($username, $password, $email = null){
        $sql = "SELECT * FROM {$this->_name} WHERE password=md5(:pass) AND ";
        if(!empty($email)){
            $sql .= "email = :email";
        }else{
            $sql .= "username = :username";
        }
        
        $query = $this->prepare($sql);
        $query->bindValue(":password", $password);
        $query->bindValue( (!empty($email)) ? ":email" : ":username" , (!empty($email)) ? $email : $username );
        $row = $query->fetchAll(PDO::FETCH_CLASS, "Model_Struct_EmployeManager");
        return $row[0];
    }
}