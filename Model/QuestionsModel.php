<?php
class Model_QuestionsModel extends Db_Database {
    protected $_name = "fb_quiz_questions";
    protected $_id = "question_id";
    protected $_primary = "fb_quiz_questions_question_id_seq";
    
    /**
     * Dodawnie nowego pytania do quizu
     * 
     * @param int $quiz_id
     * @param text $question
     * @param smallint $is_correct
     * @param int $parent_id
     * @param string $question_type
     */
    public function addQuestion($quiz_id, $question, $is_correct, $parent_id, $question_type){
        $sql = "INSERT INTO {$this->_name}(question_id, quiz_id, question, is_correct, parent_id, question_type) VALUES";
        $sql .= "(DEFAULT, :quiz_id, :question,:is_correct, :parent_id', :question_type)";
        
        $query = $this->prepare($sql);
        $query->bindValue(":quiz_id", $quiz_id, PDO::PARAM_INT);
        $query->bindValue(":question", $question, PDO::PARAM_STR);
        $query->bindValue(':is_correct', $is_correct, PDO::PARAM_INT);
        $query->bindValue(":parent_id", $parent_id, PDO::PARAM_INT);
        $query->bindValue(":question_type", $question_type, PDO::PARAM_STR);
        $query->execute();
        return $this->lastInsertId("fb_quiz_questions_question_id_seq");
    }
    
    /**
     * Pobiera pytania dot. quizu
     * @param int $quiz_id
     * @return object
     */
    public function getQuestions($quiz_id){
        $sql = "SELECT * FROM {$this->_name} WHERE parent_id=0 AND quiz_id=:quiz_id";
        $query = $this->prepare($sql);
        $query->bindValue(":quiz_id", $quiz_id, PDO::PARAM_INT);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_CLASS, "Model_Struct_QuizQuestionsModel");
    }
    
    /**
     * Pobiera zdefiniowane mozliwe odpowiedzi do pytania
     * 
     * @param int $question_id
     * @return object
     */
    public function getAnswers($parent_id){
        $sql = "SELECT * FROM {$this->_name} WHERE parent_id=:parent_id ORDER BY {$this->_id}";
        $query = $this->prepare($sql);
        $query->bindValue(":parent_id", $parent_id, PDO::PARAM_INT);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_CLASS, "Model_Struct_QuizQuestionsModel");
    }
    
    /**
     * Pobiera odpowiedz ktora wybral uzytkownik
     * 
     * @param int $question_id
     * @return object
     */
    public function getAnswer($question_id){
        $sql = "SELECT * FROM {$this->_name} WHERE question_id=:question_id";
        $query = $this->prepare($sql);
        $query->bindParam(":question_id", $question_id, PDO::PARAM_INT);
        $query->execute();
        $row = $query->fetchAll(PDO::FETCH_CLASS, "Model_Struct_QuizQuestionsModel");
        return $row[0];
    }
}
?>
