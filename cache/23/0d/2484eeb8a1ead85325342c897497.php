<?php

/* index/quiz.twig */
class __TwigTemplate_230d2484eeb8a1ead85325342c897497 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout/frontoffice.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/frontoffice.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<h3>Witamy na stronie konkursowej <i>Klubu PDP</i></h3>
<h4>Aktywne konkursu:</h4>
<ol>
    ";
        // line 8
        if (isset($context["quizes"])) { $_quizes_ = $context["quizes"]; } else { $_quizes_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($_quizes_, "getQuizes", array(), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["q"]) {
            // line 9
            echo "        <li>";
            if (isset($context["q"])) { $_q_ = $context["q"]; } else { $_q_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_q_, "quiz_name"), "html", null, true);
            echo "</li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['q'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "</ol>
";
    }

    public function getTemplateName()
    {
        return "index/quiz.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 11,  42 => 9,  37 => 8,  31 => 4,  28 => 3,);
    }
}
