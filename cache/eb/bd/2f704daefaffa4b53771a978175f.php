<?php

/* layout/frontoffice.twig */
class __TwigTemplate_ebbd2f704daefaffa4b53771a978175f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'sidebar' => array($this, 'block_sidebar'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"pl\">
\t<head>
\t</head>
\t<body>
\t\t<header>
\t\t\t";
        // line 7
        $this->displayBlock('sidebar', $context, $blocks);
        // line 8
        echo "\t\t</header>
\t\t<section id=\"content\">
\t\t\t";
        // line 10
        $this->displayBlock('content', $context, $blocks);
        // line 11
        echo "\t\t</section>
\t\t<footer>
\t\t\t<small>Power by interSHOCK IT</small>
\t\t</footer>
\t</body>
</html>";
    }

    // line 7
    public function block_sidebar($context, array $blocks = array())
    {
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "layout/frontoffice.twig";
    }

    public function getDebugInfo()
    {
        return array (  51 => 10,  46 => 7,  37 => 11,  35 => 10,  29 => 7,  21 => 1,  31 => 8,  28 => 3,);
    }
}
