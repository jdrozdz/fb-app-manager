<?php

/* layout/backoffice.twig */
class __TwigTemplate_6021dafb67fba1ba3fedd0f29b45c59b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'sidebar' => array($this, 'block_sidebar'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"pl\">
\t<head>
            <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />
            <script src=\"http://code.jquery.com/jquery-1.10.1.min.js\"  type=\"text/javascript\"></script>
            <script src=\"http://code.jquery.com/jquery-migrate-1.2.1.min.js\"  type=\"text/javascript\"></script>
            <script src=\"http://code.jquery.com/ui/1.10.3/jquery-ui.js\" type=\"text/javascript\"></script>
            <link rel=\"stylesheet\" type=\"text/css\" href=\"http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css\" />
\t</head>
\t<body>
\t\t<header>
\t\t\t";
        // line 12
        $this->displayBlock('sidebar', $context, $blocks);
        // line 13
        echo "\t\t</header>
\t\t<section id=\"content\">
\t\t\t";
        // line 15
        $this->displayBlock('content', $context, $blocks);
        // line 16
        echo "\t\t</section>
\t\t<footer>
\t\t\t<small>Power by interSHOCK IT</small>
\t\t</footer>
\t</body>
</html>";
    }

    // line 12
    public function block_sidebar($context, array $blocks = array())
    {
    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "layout/backoffice.twig";
    }

    public function getDebugInfo()
    {
        return array (  56 => 15,  51 => 12,  42 => 16,  40 => 15,  36 => 13,  34 => 12,  21 => 1,  31 => 4,  28 => 3,);
    }
}
