<?php

/* admin/quiz.twig */
class __TwigTemplate_658602083017d5ceaa55e1f16a6acd27 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout/backoffice.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/backoffice.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<h2>Nowy Quiz</h2>
<h3>Podaj nazwę nowego quizu</h3>
<dl>
    <dt>Nazwa quizu</dt>
    <dd><input type=\"text\" name=\"quiz_name\" id=\"quiz_name\" size=\"70\" placeholder=\"Wpisz tutaj nazwe quizu...\" /></dd>
    <dt>Czy quiz ma byc aktywny? <input type=\"checkbox\" name=\"enabled\" id=\"enabled\" /></dt>
    <dd><input type=\"button\" id=\"next\" value=\"Dalej\" /></dd>
</dl>

<script type=\"text/javascript\" src=\"js/questions.js\"></script>

";
    }

    public function getTemplateName()
    {
        return "admin/quiz.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,);
    }
}
