<?php

/* index/index.twig */
class __TwigTemplate_83a68f172da25e49fd418aeec2d8307b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout/frontoffice.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/frontoffice.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<h2>Quiz Klubu PDP</h2>
<h4>Aby wziąść udział w losowaniu odpowiedz na poniższe pytania</h4>
<ol>
    ";
        // line 6
        if (isset($context["quest"])) { $_quest_ = $context["quest"]; } else { $_quest_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_quest_);
        foreach ($context['_seq'] as $context["_key"] => $context["q"]) {
            // line 7
            echo "        <li>";
            if (isset($context["q"])) { $_q_ = $context["q"]; } else { $_q_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_q_, "question"), "html", null, true);
            echo "
        <ul style=\"list-style: lower-latin\">
        ";
            // line 9
            if (isset($context["questModel"])) { $_questModel_ = $context["questModel"]; } else { $_questModel_ = null; }
            if (isset($context["q"])) { $_q_ = $context["q"]; } else { $_q_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($_questModel_, "getAnswers", array(0 => $this->getAttribute($_q_, "question_id")), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                // line 10
                echo "            <li>
                <input class=\"answers\" type=\"";
                // line 11
                if (isset($context["q"])) { $_q_ = $context["q"]; } else { $_q_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_q_, "question_type"), "html", null, true);
                echo "\" value=\"";
                if (isset($context["a"])) { $_a_ = $context["a"]; } else { $_a_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_a_, "is_correct"), "html", null, true);
                echo "\" name=\"ans_";
                if (isset($context["q"])) { $_q_ = $context["q"]; } else { $_q_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_q_, "question_id"), "html", null, true);
                echo "\" /> ";
                if (isset($context["a"])) { $_a_ = $context["a"]; } else { $_a_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_a_, "question"), "html", null, true);
                echo "
            </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 14
            echo "        </ul></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['q'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "</ol>
<div class=\"clear\"></div>
<button id=\"answer_btm\">Odpowiedz!</button>
<div class=\"clear\"></div>
<script src=\"/app1/js/answers.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "index/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 16,  76 => 14,  57 => 11,  54 => 10,  48 => 9,  41 => 7,  36 => 6,  31 => 3,  28 => 2,);
    }
}
