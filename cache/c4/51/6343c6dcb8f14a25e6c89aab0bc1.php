<?php

/* admin/login.twig */
class __TwigTemplate_c4516343c6dcb8f14a25e6c89aab0bc1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout/backoffice.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/backoffice.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<table class=\"loginWindow\" align=\"center\">
    <thead>
        <tr>
            <th colspan=\"2\">Logowanie do systemu</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>
                <label for=\"username\">Login:</label>
            </th>
            <td>
                <input type=\"text\" name=\"username\" id=\"username\" />
            </td>
        </tr>
        <tr>
            <th>
                <label for=\"password\">Hasło:</label>
            </th>
            <td>
                <input type=\"password\" id=\"password\" name=\"password\" />
            </td>
        </tr>
    <tfoot>
        <tr>
            <td colspan=\"2\" align=\"center\">
                <input type=\"submit\" value=\"Zaloguj\" />
            </td>
        </tr>
    </tfoot>
    </tbody>
</table>
";
    }

    public function getTemplateName()
    {
        return "admin/login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,);
    }
}
