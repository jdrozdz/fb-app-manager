<?php

/* layout/frontoffice.twig */
class __TwigTemplate_74e6a427b49f7908a08e5e15a8fb71f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'scripts' => array($this, 'block_scripts'),
            'sidebar' => array($this, 'block_sidebar'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"pl\">
\t<head>
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
            <title>Konkurs Klubu PDP</title>
            <script src=\"http://code.jquery.com/jquery-1.10.1.min.js\"  type=\"text/javascript\"></script>
            <script src=\"http://code.jquery.com/jquery-migrate-1.2.1.min.js\"  type=\"text/javascript\"></script>
            <script src=\"http://code.jquery.com/ui/1.10.3/jquery-ui.js\" type=\"text/javascript\"></script>
            ";
        // line 9
        $this->displayBlock('scripts', $context, $blocks);
        // line 10
        echo "            <link rel=\"stylesheet\" type=\"text/css\" href=\"http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css\" />
            <link rel=\"stylesheet\" type=\"text/css\" href=\"/content/themes/default/css/layout.css\" />
\t</head>
\t<body>
\t\t<header>
\t\t\t";
        // line 15
        $this->displayBlock('sidebar', $context, $blocks);
        // line 16
        echo "\t\t</header>
\t\t<section id=\"content\">
\t\t\t";
        // line 18
        $this->displayBlock('content', $context, $blocks);
        // line 19
        echo "\t\t</section>
\t\t<footer>
\t\t\t<small>Power by interSHOCK IT</small>
\t\t</footer>
\t</body>
</html>";
    }

    // line 9
    public function block_scripts($context, array $blocks = array())
    {
    }

    // line 15
    public function block_sidebar($context, array $blocks = array())
    {
    }

    // line 18
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "layout/frontoffice.twig";
    }

    public function getDebugInfo()
    {
        return array (  68 => 18,  58 => 9,  49 => 19,  47 => 18,  43 => 16,  41 => 15,  34 => 10,  22 => 1,  92 => 19,  85 => 17,  66 => 14,  63 => 15,  57 => 12,  50 => 10,  45 => 9,  40 => 6,  37 => 5,  32 => 9,  29 => 2,);
    }
}
