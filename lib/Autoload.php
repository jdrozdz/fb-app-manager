<?php
if(session_id() == ""){
    session_start();
}
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APP_LIB),
    realpath(APP_MODEL),
    get_include_path()
)));

class Autoloader {
    private static $_instance = false;
    protected $_path;
    protected $_filename;
    protected $_className;
    protected $_ext = ".php";
    protected $PATH = array(APP_LIB, APP_MODEL, APP_CONTROLLER);
    
    public function __construct() {
        spl_autoload_register(array($this, 'load'));
    }
    public static function getInstance(){
        if(!self::$_instance){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function load($classname){
        $this->_className = $classname;
        if($this->_classExists()){
            return;
        }else{
            if($this->_fileExists()){
                return ;
            }
        }
    }


    protected function _classExists(){
        if(class_exists($this->_className)){
            return true;
        }
        return false;
    }
    
    protected function _fileExists(){
        $className = $this->_className;
        $class = preg_replace("/_/", "/", $className).".php";
        if(!file_exists($class)){
            $c = 0;
            foreach($this->PATH as $p){
                if(file_exists($p."/".$class)){
                   $c++;
                   require_once $p.'/'.$class;
                }else{
                    $c--;
                }
            }
            if($c < 0){
//                echo ("Klasa <tt>{$className}</tt> nie istnieje!");
//                echo "<pre>\n";
//                foreach ($this->PATH as $p){
//                    echo $p."/".$class."\n";
//                }
//                echo "</pre>";
            }else{
                return true;
            }
        }
    }
}

Autoloader::getInstance();

?>
