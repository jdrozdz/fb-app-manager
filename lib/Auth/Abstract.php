<?php
class Auth_Abstract {
    protected $session;
    public function __construct() {
        $this->session = new SessionManager();
    }
    
    public function __set($name, $value) {
        $this->session->{"auth"} = array($name => $value);
    }
    public function __get($name) {
        if(isset($this->session->{"auth"}[$name])){
            $x = $this->session->{"auth"}[$name];
            return $x;
        }
    }
}