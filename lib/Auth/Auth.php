<?php
class Auth_Auth extends Auth_Abstract {
    
    public function hasCredentials(){
        
    }
    
    public function setCredentials($cred = array()){
        if(!empty($cred) && is_array($cred)){
            foreach ($cred as $key => $value){
                $this->{$key} = (is_object($value)) ? serialize($value) : $value;
            }
        }elseif(!is_array($cred)){
            die("<b>[ERROR]</b> Podane parametry sa niepoprawne!<br />");
        }
    }
}
