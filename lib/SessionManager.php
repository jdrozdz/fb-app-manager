<?php

class SessionManager {
    
    private $name;
    
    public function __construct(){
        
    }
    
    public function delete($name){
        if(!empty($name)){
            if(isset($_SESSION[$name])){
                unset($_SESSION[$name]);
            }else{
                echo "<b>[Warning]</b> Nie ma takiej sesji!<br />";
            }
        }else{
            die("<b>[ERROR]</b> Nazwa sesji nie moze byc pusta!");
        }
    }
    
    public function exists($name = null){
        $this->name = ($name == null) ? $this->name : $name;
        if(isset($_SESSION[$this->name])){
            return true;
        }else{
            return false;
        }
    }
    
    public function __set($name, $value){
        if(is_object($value)){
            $val= serialize($value);
        }else{
            $val = $value;
        }
        if(!$this->exists()){
            $_SESSION[$name] = $val;
        }
    }
    
    public function __get($name){
        if(isset($_SESSION[$name])){
            $val = $_SESSION[$name];
            if(is_object($val)){
                $ret = unserialize($val);
            }else{
                $ret = $val;
            }
            
            return $ret;
        }
    }
}
