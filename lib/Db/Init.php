<?php
class Init extends PDO {
	protected $dbh;
	private static $_instance = false;
        protected $_connected = false;
	public function __construct(){
		try{
			$this->dbh = new PDO(DB_DSN, DB_USERNAME, DB_PASS);
			$this->dbh->beginTransaction();
			$this->dbh->exec("SET CLIENT_ENCODING TO '".DB_CLIENT_ENCODING."'");
                        $this->_connected = true;
		}catch(PDOException $e){
			echo $e->getMessage();
		}
	}
	
	public static function getInstance(){
		if($this->_instance === false){
			self::$_instance = new self();
		}
		return self::$_instance;
	}
}
?>