<?php
class Db_Database extends PDO {
    protected $dbh;
    protected $_connected = false;
    private static $_instance = false;
    
    public function __construct() {
        spl_autoload_register($this, array("init"));
        try{
            parent::__construct(DB_DSN, DB_USERNAME, DB_PASS);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->exec("SET CLIENT_ENCODING TO '".DB_USER_ENCODING."'");
            $this->_connected = true;
        }catch(PDOException $e){
                echo $e->getMessage();
        }
    }
    
    public static function getInstance(){
        if(self::$_instance === false){
                self::$_instance = new self();
        }
        return self::$_instance;
    }
}
?>
