CREATE TABLE fb_employe (
emploey_id SERIAL PRIMARY KEY NOT NULL,
username varchar(20) NOT NULL UNIQUE,
password varchar(100) NOT NULL,
email varchar(80) NOT NULL UNIQUE,
firstname varchar(30),
lastname varchar(30),
registered_date DATE NOT NULL DEFAULT NOW()
);

CREATE TABLE fb_quiz (
quiz_id SERIAL PRIMARY KEY NOT NULL,
quiz_name varchar(80) NOT NULL,
created_date DATE NOT NULL DEFAULT NOW(),
is_enable int2 default 1
);

CREATE TABLE fb_quiz_questions (
question_id SERIAL NOT NULL PRIMARY KEY,
quiz_id INT NOT NULL REFERENCES fb_quiz(quiz_id) ON DELETE CASCADE,
question TEXT not null,
is_correct int2 DEFAULT 0,
parent_id int,
question_type varchar(10)
);

CREATE TABLE fb_users (
user_id SERIAL PRIMARY KEY NOT NULL,
fb_name varchar(100) not null,
fb_account varchar(100) not null,
email varchar(80) not null unique,
add_date date default now()
);

create table fb_answers (
answer_id serial primary key,
quiz_id int references fb_quiz(quiz_id),
question_id int references fb_quiz_questions(question_id),
pass int2 default 0
);