<?php

abstract class Controller {
    
    private $_request_obj = array();
    protected $_request;
    
    abstract public function init();
    public function __construct() {
        spl_autoload_register($this, array("init"));
        $this->_request();
    }
    
    private function _request(){
        if(count($_GET) > 0){
            foreach ($_GET as $k => $v){
                $this->_request->{$k} = $v;
            }
        }elseif(count($_POST) > 0){
            foreach ($_POST as $k => $v){
                $this->_request->{$k} = $v;
            }
        }
    }
    
    public function __set($name, $value) {
        $this->_request_obj[$name] = $value;
    }
    
    public function __get($name) {
        if(isset($this->_request_obj[$name])){
            return $this->_request_obj[$name];
        }
    }
}
?>
