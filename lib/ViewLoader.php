<?php
Twig_Autoloader::register();

$debug = (APP_VERSION === "dev") ? true : false;
$cache = (APP_VERSION === "dev") ? APP_LIB.'/../cache' : false;

$loader = new Twig_Loader_Filesystem(APP_VIEW);
$view = new Twig_Environment($loader, array(
    'cache' => $cache,  'debug' => $debug
));
$view->addGlobal("session", $_SESSION);
// Dodawanie pluginów/dodatków
$view->addExtension(new Twig_Extension_Debug());
//$view->addExtension(new Twig_Extension_Sandbox());

?>